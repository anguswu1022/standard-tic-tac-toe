def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, num):
    print_board(board)
    print("GAME OVER")
    print(board[num], "has won")
    exit()

def is_row_winner(board, row_number):
    row_pos_1 = board[3 * row_number - 3]
    row_pos_2 = board[(3 * row_number - 3) + 1]
    row_pos_3 = board[(3 * row_number - 3) + 2]
    return row_pos_1 == row_pos_2 and row_pos_2 == row_pos_3

def is_column_winner(board, column_number):
    col_pos_1 = board[column_number - 1]
    col_pos_2 = board[column_number + 2]
    col_pos_3 = board[column_number + 5]
    return col_pos_1 == col_pos_2 and col_pos_2 == col_pos_3

# The left diagonal is 1 and the right diagonal is 2
def is_diagonal_winner(board, diagonal_number):
    dia_pos_1 = board[2 * diagonal_number - 2]
    dia_pos_2 = board[int(2 * diagonal_number - 2 + (4 * (0.5) ** (diagonal_number - 1)))]
    dia_pos_3 = board[int(2 * diagonal_number - 2 + (8 * (0.5) ** (diagonal_number - 1)))]
    return dia_pos_1 == dia_pos_2 and dia_pos_2 == dia_pos_3

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, 0)
    elif is_row_winner(board, 2):
        game_over(board, 3)
    elif is_row_winner(board, 3):
        game_over(board, 6)
    elif is_column_winner(board, 1):
        game_over(board, 0)
    elif is_column_winner(board, 2):
        game_over(board, 1)
    elif is_column_winner(board, 3):
        game_over(board, 2)
    elif is_diagonal_winner(board, 1):
        game_over(board, 0)
    elif is_diagonal_winner(board, 2):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
